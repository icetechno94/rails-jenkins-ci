FROM ruby:2.3.4
RUN apt-get update && apt-get install -y \
	build-essential \
	nodejs
RUN mkdir -p /app
WORKDIR /app
COPY Gemfile Gemfile.lock ./
RUN gem install bundler -v 1.17.3 && bundle install
COPY . ./
EXPOSE 3000