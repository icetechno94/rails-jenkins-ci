require 'rails_helper'

RSpec.describe Article, type: :model do
  let(:article) { Article.new(attributes) }

  describe 'article creation' do

    context 'saved with name and content' do
      let(:attributes) { { name: 'name', content: 'content' } }
      it 'without errors' do
        expect(article.save).to be_truthy
      end
    end

    context 'saved without name' do
      let(:attributes) { { content: 'content' } }
      it 'with errors' do
        expect(article.save).to be_falsey
      end
    end

    context 'with name' do
      let(:attributes) { { name: 'name' } }

      before do
        article.save!
      end
      it 'not saved in database' do
        expect(Article.exists?).to be_truthy
      end
    end

    context 'with name and content' do
      let(:attributes) { { name: 'name', content: 'content' } }

      before do
        article.save
      end
      it 'saved in database' do
        expect(Article.exists?).to be_truthy
      end

      it 'attributes saved properly' do
        expect(Article.first&.attributes&.except('_id').symbolize_keys).to eq(attributes)
      end
    end
  end
end
